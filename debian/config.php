<?php


// Standard Debian paths
define('DATA_DIR', '/var/lib/kanboard/data');
define('PLUGINS_DIR', '/var/lib/kanboard/plugins');
define('CACHE_DIR', '/var/cache/kanboard');
define('FILES_DIR', '/var/lib/kanboard/files');

// Standard Debian Mail configuration
define('MAIL_CONFIGURATION', true);
define('MAIL_FROM', 'kanboard-notifications@localhost.localdomain');
define('MAIL_TRANSPORT', 'sendmail');
define('MAIL_SENDMAIL_COMMAND', '/usr/sbin/sendmail -t');

// Enable/disable the reverse proxy authentication
define('REVERSE_PROXY_AUTH', false);

// Header name to use for the username
define('REVERSE_PROXY_USER_HEADER', 'REMOTE_USER');

// Username of the admin, by default blank
define('REVERSE_PROXY_DEFAULT_ADMIN', '');

// Header name to use for the username
define('REVERSE_PROXY_EMAIL_HEADER', 'REMOTE_EMAIL');

// Default domain to use for setting the email address
define('REVERSE_PROXY_DEFAULT_DOMAIN', '');

// Enable or disable "Strict-Transport-Security" HTTP header
define('ENABLE_HSTS', true);

// Enable or disable "X-Frame-Options: DENY" HTTP header
define('ENABLE_XFRAME', true);

// Escape html inside markdown text
define('MARKDOWN_ESCAPE_HTML', true);

// Enable/disable url rewrite
define('ENABLE_URL_REWRITE', true);

// Enable captcha after 3 authentication failure
define('BRUTEFORCE_CAPTCHA', 3);

// Lock the account after 6 authentication failure
define('BRUTEFORCE_LOCKDOWN', 6);

// Lock account duration in minute
define('BRUTEFORCE_LOCKDOWN_DURATION', 15);

// Session handler: db or php
define('SESSION_HANDLER', 'php');
