#
# Regular cron jobs for the kanboard package
#

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user command
11  3  *   *   *  www-data [ ! -d /run/systemd/system ] && [ -x /usr/share/kanboard/cli ] && /usr/share/kanboard/cli cronjob
